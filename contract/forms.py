from django import forms
from django.forms.formsets import BaseFormSet
from django.forms import ModelForm

class NameForm(forms.Form):
	your_name = forms.CharField(label='Your name', max_length=100, required=False)

class ContactForm(forms.Form):
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)

class ProfileForm(forms.Form):

    # Propietarios 
    nombre_propietario1 = forms.CharField(label='Nombre Completo de Propietario/a/s', max_length=250, required=False)
    dni_propietario1 = forms.CharField(label='DNI de pripietario', max_length=250, required=False)
    dirrecion_propietario1 = forms.CharField(label='Dirección del propietario', max_length=250, required=False)

    nombre_propietario2 = forms.CharField(label='Nombre Completo de Propietario/a/s', max_length=250, required=False)
    dni_propietario2 = forms.CharField(label='DNI de pripietario', max_length=250, required=False)
    dirrecion_propietario2 = forms.CharField(label='Dirección del propietario', max_length=250, required=False)

    nombre_propietario3 = forms.CharField(label='Nombre Completo de Propietario/a/s', max_length=250, required=False)
    dni_propietario3 = forms.CharField(label='DNI de pripietario', max_length=250, required=False)
    dirrecion_propietario3 = forms.CharField(label='Dirección del propietario', max_length=250, required=False)

    nombre_propietario4 = forms.CharField(label='Nombre Completo de Propietario/a/s', max_length=250, required=False)
    dni_propietario4 = forms.CharField(label='DNI de pripietario', max_length=250, required=False)
    dirrecion_propietario4 = forms.CharField(label='Dirección del propietario', max_length=250, required=False)

    nombre_propietario5 = forms.CharField(label='Nombre Completo de Propietario/a/s', max_length=250, required=False)
    dni_propietario5 = forms.CharField(label='DNI de pripietario', max_length=250, required=False)
    dirrecion_propietario5 = forms.CharField(label='Dirección del propietario', max_length=250, required=False)

    # Inquilinos
    nombre_inquilino1 = forms.CharField(label='Nombre Completo de Inquilino/a/s', max_length=250, required=False)
    tipo_documento1 = forms.CharField(label='Tipo del Documento', max_length=250, required=False)
    numero_documento1 = forms.CharField(label='Número del documento', max_length=250, required=False)
    dirrecion_inquilino1 = forms.CharField(label='Dirreción de inquilino', max_length=250, required=False)

    nombre_inquilino2 = forms.CharField(label='Nombre Completo de Inquilino/a/s', max_length=250, required=False)
    tipo_documento2 = forms.CharField(label='Tipo del Documento', max_length=250, required=False)
    numero_documento2 = forms.CharField(label='Número del documento', max_length=250, required=False)
    dirrecion_inquilino2 = forms.CharField(label='Dirreción de inquilino', max_length=250, required=False)

    nombre_inquilino3 = forms.CharField(label='Nombre Completo de Inquilino/a/s', max_length=250, required=False)
    tipo_documento3 = forms.CharField(label='Tipo del Documento', max_length=250, required=False)
    numero_documento3 = forms.CharField(label='Número del documento', max_length=250, required=False)
    dirrecion_inquilino3 = forms.CharField(label='Dirreción de inquilino', max_length=250, required=False)

    nombre_inquilino4 = forms.CharField(label='Nombre Completo de Inquilino/a/s', max_length=250, required=False)
    tipo_documento4 = forms.CharField(label='Tipo del Documento', max_length=250, required=False)
    numero_documento4 = forms.CharField(label='Número del documento', max_length=250, required=False)
    dirrecion_inquilino4 = forms.CharField(label='Dirreción de inquilino', max_length=250, required=False)

    # Other info

    calle = forms.CharField(label='Dirección completa del departamento a Alquiler', max_length=250, required=False)
    fecha_comienzo = forms.DateField(label='Fecha comienzo contrato', widget = forms.SelectDateWidget, required=True)
    fecha_ingreso = forms.DateField(label='Fecha de ingreso (si no hay proporcional, deja vacío)', widget = forms.SelectDateWidget, required=False)
    num_props = forms.IntegerField(label='Número de Propietarios', required=False)
    num_inq = forms.IntegerField(label='Número de Inquilinos', required=False)
    
    
    texto_monto = forms.CharField(label='Mostrar en texto monto del contrato', max_length=250, required=False)
    valor_monto = forms.CharField(label='Mostrar valor del contrato', max_length=250, required=False)

    perct = '%'
    alquiler = forms.FloatField(label='Valor de Alquiler', required=False)
    ajuste = forms.FloatField(label='Ajuste (12.50{} sería 12.50)'.format(perct), required=False)
    semestral_o_anual = forms.BooleanField(label='Si SEMESTRAL, marca esta casilla. Si ANUAL, dejalo vacío.', required=False)

    nombre_garante = forms.CharField(label='Nombre de garante: si eligiste 1 arriba', max_length=250, required=False)
    matricula = forms.CharField(label='Matrícula FR (.../..): si eligiste 1 arriba', max_length=250, required=False)
    numero_poliza = forms.CharField(label='Número de póliza: si eligiste 2-5 arriba', max_length=250, required=False)
    nombre_finaer = forms.CharField(label='Nombre de representante de Finaer: si eligiste 6 arriba', max_length=250, required=False)
    documento_representanto = forms.CharField(label='Número de documento representante: si eligiste 6 arriba', max_length=250, required=False)

    numero_monto_deposito = forms.FloatField(label='Mostrar monto en número del depósito', required=False)

    option1 = forms.BooleanField(label='1) Garantía Propietaria', required=False)
    option2 = forms.BooleanField(label='2) SMG Compañía Argentina de Seguros SA', required=False)
    option3 = forms.BooleanField(label='3) Fianzas y Crédito S.A. Cia. De Seguros', required=False)
    option4 = forms.BooleanField(label='4) Aseguradora PROVINCIA SEGUROS S.A.', required=False)
    option5 = forms.BooleanField(label='5) Alba Caución Compañía Argentina de Seguros', required=False)
    option6 = forms.BooleanField(label='6) Sistema Finaer S.A.', required=False)



