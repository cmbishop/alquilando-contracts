from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from .models import Choice, Question
from .forms import NameForm, ProfileForm
from django.core.mail import send_mail
import sys
from fpdf import FPDF
from django.views.static import serve
import os
import sys
from fpdf import FPDF
import datetime
from dateutil.relativedelta import relativedelta
from num2words import num2words
import pdfkit
from xhtml2pdf import pisa


def index(request): 
    # Form handling
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            clean = form.cleaned_data

            #### PDF CONSTRUCTION #### 
            # Initializes variables with  data
            # Propietario
            nombre_propietario1 = clean.get('nombre_propietario1')
            dni_propietario1 = clean.get('dni_propietario1')
            dirrecion_propietario1 = clean.get('dirrecion_propietario1')

            nombre_propietario2 = clean.get('nombre_propietario2')
            dni_propietario2 = clean.get('dni_propietario2')
            dirrecion_propietario2 = clean.get('dirrecion_propietario2')

            nombre_propietario3 = clean.get('nombre_propietario3')
            dni_propietario3 = clean.get('dni_propietario3')
            dirrecion_propietario3 = clean.get('dirrecion_propietario3')

            nombre_propietario4 = clean.get('nombre_propietario4')
            dni_propietario4 = clean.get('dni_propietario4')
            dirrecion_propietario4 = clean.get('dirrecion_propietario4')

            nombre_propietario5 = clean.get('nombre_propietario5')
            dni_propietario5 = clean.get('dni_propietario5')
            dirrecion_propietario5 = clean.get('dirrecion_propietario5')

            propietarios = ""
            if nombre_propietario5:
                propietarios = "{} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}".format(nombre_propietario1, dni_propietario1, dirrecion_propietario1, nombre_propietario2, dni_propietario2, dirrecion_propietario2, nombre_propietario3, dni_propietario3, dirrecion_propietario3, nombre_propietario4, dni_propietario4, dirrecion_propietario4, nombre_propietario5, dni_propietario5, dirrecion_propietario5)
            elif nombre_propietario4:
                propietarios = "{} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}".format(nombre_propietario1, dni_propietario1, dirrecion_propietario1, nombre_propietario2, dni_propietario2, dirrecion_propietario2, nombre_propietario3, dni_propietario3, dirrecion_propietario3, nombre_propietario4, dni_propietario4, dirrecion_propietario4)
            elif nombre_propietario3:
                propietarios = "{} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}".format(nombre_propietario1, dni_propietario1, dirrecion_propietario1, nombre_propietario2, dni_propietario2, dirrecion_propietario2, nombre_propietario3, dni_propietario3, dirrecion_propietario3)
            elif nombre_propietario2:
                propietarios = "{} con DNI {} con domicilio en {}, {} con DNI {} con domicilio en {}".format(nombre_propietario1, dni_propietario1, dirrecion_propietario1, nombre_propietario2, dni_propietario2, dirrecion_propietario2)
            elif nombre_propietario1:
                propietarios = "{} con DNI {} con domicilio en {}".format(nombre_propietario1, dni_propietario1, dirrecion_propietario1)
                
            # Inquilino
            nombre_inquilino1 = clean.get('nombre_inquilino1')
            tipo_documento1 = clean.get('tipo_documento1')
            numero_documento1 = clean.get('numero_documento1')
            dirrecion_inquilino1 = clean.get('dirrecion_inquilino1')

            nombre_inquilino2 = clean.get('nombre_inquilino2')
            tipo_documento2 = clean.get('tipo_documento2')
            numero_documento2 = clean.get('numero_documento2')
            dirrecion_inquilino2 = clean.get('dirrecion_inquilino2')

            nombre_inquilino3 = clean.get('nombre_inquilino3')
            tipo_documento3 = clean.get('tipo_documento3')
            numero_documento3 = clean.get('numero_documento3')
            dirrecion_inquilino3 = clean.get('dirrecion_inquilino3')

            nombre_inquilino4 = clean.get('nombre_inquilino4')
            tipo_documento4 = clean.get('tipo_documento4')
            numero_documento4 = clean.get('numero_documento4')
            dirrecion_inquilino4 = clean.get('dirrecion_inquilino4')

            inquilinos = ""
            if nombre_inquilino4:
                inquilinos = "{} con número de {} {} con domicilio Real en {}, {} con número de {} {} con domicilio Real en {}, {} con número de {} {} con domicilio Real en {}, {} con número de {} {} con domicilio Real en {}".format(nombre_inquilino1, tipo_documento1, numero_documento1, dirrecion_inquilino1, nombre_inquilino2, tipo_documento2, numero_documento2, dirrecion_inquilino2, nombre_inquilino3, tipo_documento3, numero_documento3, dirrecion_inquilino3, nombre_inquilino4, tipo_documento4, numero_documento4, dirrecion_inquilino4)
            elif nombre_inquilino3:
                inquilinos = "{} con número de {} {} con domicilio Real en {}, {} con número de {} {} con domicilio Real en {}, {} con número de {} {} con domicilio Real en {}".format(nombre_inquilino1, tipo_documento1, numero_documento1, dirrecion_inquilino1, nombre_inquilino2, tipo_documento2, numero_documento2, dirrecion_inquilino2, nombre_inquilino3, tipo_documento3, numero_documento3, dirrecion_inquilino3)
            elif nombre_inquilino2:
                inquilinos = "{} con número de {} {} con domicilio Real en {}, {} con número de {} {} con domicilio Real en {}".format(nombre_inquilino1, tipo_documento1, numero_documento1, dirrecion_inquilino1, nombre_inquilino2, tipo_documento2, numero_documento2, dirrecion_inquilino2)
            elif nombre_inquilino1:
                inquilinos = "{} con número de {} {} con domicilio Real en {}".format(nombre_inquilino1, tipo_documento1, numero_documento1, dirrecion_inquilino1)

            alquiler = clean.get('alquiler')
            ajuste = clean.get('ajuste')
            # Initialize dummy data for testing
            if not alquiler:
                alquiler = 8000
            else:
                alquiler = float(alquiler)
            if not ajuste:
                ajuste = 12
            else:
                ajuste = float(ajuste)

            fecha_comienzo = clean.get('fecha_comienzo')
            fecha_fin = fecha_comienzo + relativedelta(months=+24) - relativedelta(days=+1)
            nombre_garante = clean.get('nombre_garante')
            matricula = clean.get('matricula')
            calle = clean.get('calle')
            proporcional = clean.get("proporcional")
            xx = clean.get('fecha_comienzo').strftime('%d')
            numero_poliza = clean.get('numero_poliza')
            if not numero_poliza:
                numero_poliza = "_______"
            mes = clean.get('fecha_comienzo').strftime('%m')
            if mes == '01':
                mes = "Enero"
            if mes == '02':
                mes = "Febrero"
            if mes == '03':
                mes = "Marzo"
            if mes == '04':
                mes = "Abril"
            if mes == '05':
                mes = "Mayo"
            if mes == '06':
                mes = "Junio"
            if mes == '07':
                mes = "Julio"
            if mes == '08':
                mes = "Agosto"
            if mes == '09':
                mes = "Septiembre"
            if mes == '10':
                mes = "Octubre"
            if mes == '11':
                mes = "Noviembre"
            if mes == '12':
                mes = "Diciembre"

            ano = clean.get('fecha_comienzo').strftime('%Y')

            nombre_finaer = clean.get('nombre_finaer')
            if not clean.get('numero_monto_deposito'):
                numero_monto_deposito = 1000
            else:
                numero_monto_deposito = clean.get('numero_monto_deposito')
            texto_monto_deposito = num2words(numero_monto_deposito, to='currency', lang='es_CO') 
            fianza_o_garantia = clean.get('fianza_o_garantia')
            documento_representanto = clean.get('documento_representanto')
            option = 1
            fianza_o_garantia = "- LA CLÁUSULA DE: GARANTÍA PROPIETARIA"
            if clean.get('option1'):
                option = 1
                fianza_o_garantia = "- LA CLÁUSULA DE: GARANTÍA PROPIETARIA"
            if clean.get('option2'):
                option = 2
                fianza_o_garantia = "- LA CLÁUSULA DE: FIANZA"
            if clean.get('option3'):
                option = 3
                fianza_o_garantia = "- LA CLÁUSULA DE: FIANZA"
            if clean.get('option4'):
                option = 4
                fianza_o_garantia = "- LA CLÁUSULA DE: FIANZA"
            if clean.get('option5'):
                option = 5
                fianza_o_garantia = "- LA CLÁUSULA DE: FIANZA"
            if clean.get('option6'):
                option = 6
                fianza_o_garantia = "- SISTEMA FINAER"

            semestral_o_anual = clean.get('semestral_o_anual')

            ################### CALCULATES PROPORTIONAL INFORMATION ###################
            fecha_ingreso = clean.get('fecha_ingreso')
            if not fecha_ingreso:
                fecha_ingreso = fecha_comienzo
            else:  
                fecha_ingreso = clean.get('fecha_ingreso')


            dias_mes = (fecha_comienzo - fecha_ingreso).days
            valor_proporcional = int(round((alquiler*dias_mes)/ 30))
            texto_proporcional = num2words(valor_proporcional, to='currency', lang='es_CO') 

            ################### CALCULATES SEMESTRAL INFORMATION ###################

            # Works with alquiler y ajuste y fecha comienza.

            ajuste = ajuste/100

            # Semester periods
            sem1_start = fecha_comienzo
            sem2_start = fecha_comienzo + relativedelta(months=+6)
            sem3_start = fecha_comienzo + relativedelta(months=+12)
            sem4_start = fecha_comienzo + relativedelta(months=+18)
            sem5_start = fecha_comienzo + relativedelta(months=+24)
            sem1_end = sem2_start - datetime.timedelta(days=1)
            sem2_end = sem3_start - datetime.timedelta(days=1)
            sem3_end = sem4_start - datetime.timedelta(days=1)
            sem4_end = sem5_start - datetime.timedelta(days=1)
            semestre1 = "{} hasta {}".format(sem1_start.strftime('%d-%m-%Y'), sem1_end.strftime('%d-%m-%Y'))
            semestre2 = "{} hasta {}".format(sem2_start.strftime('%d-%m-%Y'), sem2_end.strftime('%d-%m-%Y'))
            semestre3 = "{} hasta {}".format(sem3_start.strftime('%d-%m-%Y'), sem3_end.strftime('%d-%m-%Y'))
            semestre4 = "{} hasta {}".format(sem4_start.strftime('%d-%m-%Y'), sem4_end.strftime('%d-%m-%Y'))
            ano1 = "{} hasta {}".format(sem1_start.strftime('%d-%m-%Y'), sem2_end.strftime('%d-%m-%Y'))
            ano2 = "{} hasta {}".format(sem3_start.strftime('%d-%m-%Y'), sem4_end.strftime('%d-%m-%Y'))

            numeros_per_semestre1 = alquiler
            numeros_per_semestre2 = numeros_per_semestre1*ajuste + numeros_per_semestre1
            numeros_per_semestre3 = numeros_per_semestre2*ajuste + numeros_per_semestre2
            numeros_per_semestre4 = numeros_per_semestre3*ajuste + numeros_per_semestre3

            numeros_per_semestre1b = int(numeros_per_semestre1)
            numeros_per_semestre2b = int(numeros_per_semestre2)
            numeros_per_semestre3b = int(numeros_per_semestre3)
            numeros_per_semestre4b = int(numeros_per_semestre4)
            numeros_per_ano1 = numeros_per_semestre1 + numeros_per_semestre2
            numeros_per_ano2 = numeros_per_semestre3 + numeros_per_semestre4


            texto_per_semestre1 = num2words(alquiler, to='currency', lang='es_CO')
            texto_per_semestre2 = num2words(numeros_per_semestre2b, to='currency', lang='es_CO')
            texto_per_semestre3 = num2words(numeros_per_semestre3b, to='currency', lang='es_CO')
            texto_per_semestre4 = num2words(numeros_per_semestre4b, to='currency', lang='es_CO')
            texto_per_ano1 = num2words(numeros_per_ano1, to='currency', lang='es_CO')
            texto_per_ano2 = num2words(numeros_per_ano2, to='currency', lang='es_CO')

            
            valor_monto = int((numeros_per_semestre1 + numeros_per_semestre2 + numeros_per_semestre3 + numeros_per_semestre4)*6)
            texto_monto = num2words(valor_monto, to='currency', lang='es_CO')

            numeros_per_semestre1 = int(numeros_per_semestre1)
            numeros_per_semestre2 = int(numeros_per_semestre2)
            numeros_per_semestre3 = int(numeros_per_semestre3)
            numeros_per_semestre4 = int(numeros_per_semestre4)
            ################### CREATES PDF ###################

             # Prints the intro paragraph

            textIntro = u"Entre <b>{}</b>,​ en adelante llamado <b>\"EL LOCADOR\"</b> ; por una parte, y por la otra <b>{}</b>, y domicilio​ Especial en calle <b>{}</b> en adelante llamado <b>\"EL LOCATARIO\"</b>,​ ambos mayores de edad y hábiles para contratar han convenido en celebrar el presente contrato de locación sujeto a las siguientes condiciones y cláusulas. ".format(propietarios, inquilinos, calle)

            # Primera
            text1 = u"<b><u>PRIMERA - OBJETO: El LOCADOR​</b></u> da en locación a <b>la LOCATARIA</b>​ y ésta recibe de conformidad, el inmueble ubicado en​ <b>{}</b>, cuyo contenido y estado de conservación <b>EL LOCATARIO</b> conoce y acepta. <b>\"EL​ LOCATARIO\"</b> se compromete formalmente a mantenerlos en el mismo estado en que los recibe permitiendo el acceso mensualmente con previo aviso al <b>LOCADOR​</b> y se obliga a pagar cuando desocupe el bien locado el importe de los objetos que faltaren y/o los desperfectos ocasionados cuyo valor será fijado únicamente por <b>\"EL​ LOCADOR\"</b> según los precios de plaza en ese momento. Se conviene formalmente que <b>\"EL​ LOCATARIO\"</b> se obliga al pago del alquiler hasta el día que <b>\"EL​ LOCADOR\"</b> reciba el bien locado en las debidas condiciones y a total conformidad de este último. Queda establecido que <b>“EL LOCATARIO”</b> no podrá agujerear ni perforar azulejos y cerámicos ni sus juntas, ya que la unidad se entrega sin perforaciones en azulejos y cerámicos. <b>\"EL​ LOCATARIO\"</b> no se hace responsable ante <b>\"EL​ LOCADOR\"</b> por el desgaste ocasionado por el tiempo y el buen uso. ".format(calle)

            # Segunda
            if fecha_ingreso != fecha_comienzo:
                text2 = u"<b> <u>SEGUNDA - PLAZO:</b></u> La duración o término del presente contrato ha sido fijado en <b>DOS AÑOS (2)</b> años a​ contar del <b>{}</b>,​ venciendo en consecuencia el <b>{}</b>. Por acuerdo entre partes, el ingreso de <b>\"EL LOCATARIO\"</b> es el <b>{}</b>, abonando el proporcional a <b>{}</b> días del primer mes, por la suma fija de <b>{}</b>,​  ​ (​$<b>{}</b>)".format(fecha_comienzo.strftime('%d-%m-%Y'), fecha_fin.strftime('%d-%m-%Y'), fecha_ingreso.strftime('%d-%m-%Y'), dias_mes, texto_proporcional, valor_proporcional)
            else:
                text2 = u"<b> <u>SEGUNDA - PLAZO:</b></u> La duración o término del presente contrato ha sido fijado en <b>DOS AÑOS (2)</b> años a​ contar del <b>{}</b>,​ venciendo en consecuencia el <b>{}</b>.".format(fecha_comienzo.strftime('%d-%m-%Y'), fecha_fin.strftime('%d-%m-%Y'))
            
            # Tercera
            text3 = u"<b><u>TERCERA - DESTINO:</u> \"EL LOCATARIO\"</b> destinará el inmueble locado exclusivamente para vivienda propia y de sus familiares directos."

            # Cuarta
            if semestral_o_anual:
                text4 = u'''<b><u>CUARTA - PRECIO:</b></u> El precio de la presente locación se fija en la de PESOS​ <b>{}</b>  ($<b>{}-</b>) por​ todo el período de vigencia de contrato, pagaderos de común acuerdo entre <b>\"EL​ LOCADOR\"</b> y <b>\"EL​ LOCATARIO\"</b> de la siguiente manera: la suma de PESOS​ <b>{}</b> (​ $<b>{}</b> )​ mensuales, durante el período comprendido entre <b>{}</b>; la suma de PESOS ​<b>{}</b>  (​ $<b>{}</b> )​ mensuales, durante el período comprendido entre el <b>{}</b> ; la suma de PESOS <b>{}</b> (​ $<b>{}</b> )​  mensuales, durante el período comprendido entre el <b>{}</b> y la suma de PESOS​ <b>{}</b> (​ $<b>{}</b> )​ mensuales, durante el período comprendido entre el <b>{}</b>. Todos los pagos se efectuarán por mes adelantado, hasta la finalización del contrato, del día 1 al día 10 de cada mes, Por depósito bancario a la cuenta que indique LOCADOR O donde <b>\"EL​ LOCADOR\"</b> lo indique fehacientemente en el futuro. La mora en el pago de los alquileres, se producirá en forma automática por el mero transcurso del tiempo y sin necesidad de interpelación ni gestión previa de ninguna naturaleza. Producida la mora los alquileres siempre deberán abonarse con un interés compensatorio del <b>tres (3%)</b> por ciento mensual acumulativo y un interés punitorio del <b>dos (2) por ciento mensual acumulativo</b>. De verse obligado <b>\"EL LOCADOR\"</b> a​ intimar a <b>\"EL​ LOCATARIO\"</b> para obtener el cobro de las obligaciones contraídas, este último deberá abonar además del alquiler pactado los gastos y honorarios que se devengaren por dicha intimación.'''.format(texto_monto, valor_monto, texto_per_semestre1, numeros_per_semestre1, semestre1, texto_per_semestre2, numeros_per_semestre2, semestre2, texto_per_semestre3, numeros_per_semestre3, semestre3, texto_per_semestre4, numeros_per_semestre4, semestre4)
            else: 
                text4 = u'''<b><u>CUARTA - PRECIO:</b></u> El precio de la presente locación se fija en la de PESOS​ {}  (${}-) por​ todo el período de vigencia de contrato, pagaderos de común acuerdo entre <b>“EL​ LOCADOR”</b> y <b>"EL​ LOCATARIO"</b> de la siguiente manera: la suma de PESOS​ {} (​ ${} )​ mensuales, durante el período comprendido entre {}; la suma de PESOS ​{}  (​ ${} )​ mensuales, durante el período comprendido entre el {}. Todos los pagos se efectuarán por mes adelantado, hasta la finalización del contrato, del día 1 al día 10 de cada mes, Por depósito bancario a la cuenta que indique <b>LOCADOR</b> o donde <b>\"EL​ LOCADOR\"</b> lo indique fehacientemente en el futuro. La mora en el pago de los alquileres, se producirá en forma automática por el mero transcurso del tiempo y sin necesidad de interpelación ni gestión previa de ninguna naturaleza. Producida la mora los alquileres siempre deberán abonarse con un interés compensatorio del <b>tres (3%) por ciento mensual</b> acumulativo y un interés punitorio del <b>dos (2) por ciento mensual</b> acumulativo. De verse obligado <b>"EL LOCADOR"</b> a​ intimar a <b>"EL​ LOCATARIO"</b> para obtener el cobro de las obligaciones contraídas, este último deberá abonar además del alquiler pactado los gastos y honorarios que se devengaren por dicha intimación.'''.format(texto_monto, valor_monto, texto_per_ano1, numeros_per_ano1, ano1, texto_per_ano2, numeros_per_ano2, ano2)
            
            # Quinta
            text5 = u"<b><u>QUINTA - INTRANSFERIBILIDAD:</b></u> Están​ prohibidas cualquier sublocación y/o transmisión parcial o total, transitoria o permanente, gratuita u onerosa, y en general, a todo título, el cambio del destino habitacional y/o a faltar a la moral o buenas costumbres."

            # Sexta
            text6 = u"<b><u>SEXTA - MEJORAS:</b></u> Queda terminantemente prohibido a <b>\"EL​ LOCATARIO\"</b> realizar mejoras o introducir modificaciones de cualquier clase en el bien locado, sin la autorización de <b>\"EL​ LOCADOR\"</b> dada en forma expresa y por escrito. Todas las mejoras que se introduzcan quedarán en beneficio de la propiedad y no podrá <b>\"EL​ LOCATARIO\"</b> reclamar indemnización alguna."

            # Septima
            text7 = u"<b><u>SÉPTIMA - CONSERVACIÓN:</b></u> Serán por cuenta de <b>\"EL​ LOCATARIO\"</b> la conservación de los artefactos y accesorios de la propiedad, y las reparaciones de los desperfectos provocados por el uso. <b>\"EL​ LOCATARIO\"</b> dará inmediata cuenta a <b>\"EL​ LOCADOR\"</b> de cualquier desperfecto que sufriera la propiedad permitiendo al mismo o a sus representantes el libre acceso a cualquier dependencia de la misma cuando éste lo juzgue necesario para su inspección y permitirá la ejecución de todo trabajo que sea necesario para su conservación sin derecho de cobrar indemnización alguna."

            # Octava 
            text8 = u"<b><u>OCTAVA - RESPONSIBILIDAD:</u>\"EL LOCADOR\"</b> no se hace responsable por los accidentes y/o daños que pudiera sufrir <b>\"EL​ LOCATARIO\"</b> y/o​ los bienes depositados en el inmueble en caso de robo, hurto, rotura de caños, inundaciones, incendio, desprendimientos, o cualquier otro accidente que pudiera producirse por cualquier causa. <b>\"EL​ LOCATARIO\"</b> toma a su cargo el caso fortuito o fuerza mayor. <b>\"EL​ LOCATARIO\"</b> responderá asimismo por la ruina total o parcial de la cosa locada por explosión de algún artefacto que introduzcan en lo locado."

            # Novena
            text9 = u"<b><u>NOVENA - CONSIGNACIÓN:</u></b> En​ caso de consignación de llaves o alquileres, el alquiler regirá hasta el día que <b>\"EL​ LOCADOR\"</b> tome posesión real y efectiva de la propiedad."

            # Decima
            text10 = u"<b><u>DÉCIMA - MULTA:</b></u> Si <b>\"EL​ LOCATARIO\"</b> diera motivo por cualquier causa a que se le iniciare juicio de desalojo se compromete a pagar desde la iniciación del juicio y hasta que <b>\"EL​ LOCADOR\"</b> reciba las llaves del bien locado, la suma de pesos Mil ($ 1000.-) diarios además del alquiler pactado."

            # Decima Primera
            text11 = u"<b><u>DÉCIMA PRIMERA- CARGAS:</u> \"El LOCATARIO\"</b> asume la obligación del pago puntual de expensas ordinarias y los servicios de electricidad, agua, gas natural y póliza cualquier otro servicio que contratase. Todos los pagos serán realizados por <b>\"El​ LOCATARIO\"</b> dentro de las fechas correspondientes a los primeros vencimientos. El pago se acreditará por con entrega de los recibos originales cancelatorios con constancia de pago, al <b>\"LOCADOR\"</b>,​ dentro del mes correspondiente a los mismos, conforme arts. <b>1209 y 1210 del Código Civil y Comercial de la Nación.</b>"

            # Decima Segunda
            if option == 1:

                toInsert = u"<b><u>DÉCIMA SEGUNDA{}:</u></b> Para el fiel cumplimiento de este contrato el señor/a <b>{}</b>, D.N.I. N° <b>{}</b>, domiciliado en <b>{}</b>, quien declara conocer y aceptar todas las cláusulas del presente contrato en prueba de lo cual lo firma de conformidad se constituye en fiador solidario y principal pagador de todas y cada una de las obligaciones de este contrato y sus accesorios que sean a cargo de <b>EL LOCATARIO</b>, renunciando a los beneficios de división y excusión de bienes. Sin perjuicio de responder con todos sus bienes el señor/a <b>{}</b> garantiza esta locación con el inmueble de su propiedad sito en <b>{} {}</b> manifestando que dicha propiedad no está ni será colocada como bien de familia, y/o protección similar, ni reconoce gravámenes, restricciones e interdicciones al día de la firma del presente, comprometiéndose a no vender ni hipotecar la misma, en tal supuesto, <b>“EL LOCADOR”</b> podrá solicitar se le presente otra garantía a satisfacción en el término de <b>diez (10) días</b> bajo apercibimiento de considerar resuelto el presente contrato. El fiador manifiesta conocer y aceptar que será responsable en los términos del art. <b>172 del Código Penal</b>, para el supuesto de vender o gravar de cualquier modo sus bienes, tornándose insolventes fraudulentamente.".format(fianza_o_garantia, nombre_inquilino1, numero_documento1, calle, nombre_garante, calle, matricula)

                text12 = '''
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                '''.format(toInsert)
            elif option == 2:
                toInsert = u"<b><u>DÉCIMA SEGUNDA{}:</u></b> En garantía del fiel cumplimiento del pago de los alquileres indicados en este contrato, <b>la LOCATARIA</b> ofrece una póliza de caución de <b>SMG Compañía Argentina de Seguros SA</b>, con domicilio en <b>Av. Corrientes 1865 PB. - Ciudad Autónoma de Buenos Aires</b>, N° de póliza <b>{}</b> con vigencia a partir del <b>{} de {} {}</b>, a favor de <b>la LOCADORA</b> por el monto y duración del contrato. En virtud de ello, <b>la LOCATARIA</b> ofrece a <b>la LOCADORA</b>, y esta acepta, la garantía referida de acuerdo a las siguientes condiciones: una póliza de caución de <b>SMG Compañía Argentina de Seguros SA</b>. a favor de <b>la LOCADORA</b> que garantizará el pago de los alquileres emergentes del presente contrato durante el término de <b>los 24 meses</b> de duración de la locación.".format(fianza_o_garantia, numero_poliza, xx, mes, ano)
                text12 = '''
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                '''.format(toInsert)
            elif option == 3:
                toInsert = u"<b><u>DÉCIMA SEGUNDA{}:</u></b> En garantía del fiel cumplimiento del pago de los alquileres indicados en este contrato, <b>la LOCATARIA</b> ofrece una póliza de caución de <b>la Fianzas y Crédito S.A</b>. Cia. De Seguros, con domicilio en <b>Av. Leandro N. Alem 855 Piso 10° - Ciudad Autónoma de Buenos Aires</b>, N° de póliza <b>{}</b> con vigencia a partir del <b>{} de {} {}</b>, a favor de <b>la LOCADORA</b> por el monto y duración del contrato. En virtud de ello, <b>la LOCATARIA</b> ofrece a <b>la LOCADORA</b>, y esta acepta, la garantía referida de acuerdo a las siguientes condiciones: una póliza de caución de <b>Fianzas y Crédito S.A. Cia</b>. De Seguros a favor de <b>la LOCADORA</b> que garantizará el pago de los alquileres emergentes del presente contrato durante el término de <b>los 24 meses</b> de duración de la locación.".format(fianza_o_garantia, numero_poliza, xx, mes, ano)
                text12 = '''
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                '''.format(toInsert)
            elif option == 4:
                toInsert = u"<b><u>DÉCIMA SEGUNDA{}:</u></b> El cumplimiento de las obligaciones del <b>LOCATARIO</b> se garantiza mediante la Póliza de Caución Nro. <b>{}</b>, emitida por la Aseguradora <b>PROVINCIA SEGUROS S.A.</b>, en lo referido a las siguientes cargas: Suma mensual del alquiler conforme contrato, desde el momento en que se haya dejado de abonar y hasta la desocupación del inmueble por parte del <b>LOCATARIO</b> o vencimiento de contrato, lo que ocurra primero; Depósito en garantía hasta un máximo <b>de 2 meses</b> de alquiler en exceso de la suma entregada por el <b>LOCATARIO al LOCADOR</b> por este concepto; Pago de <b>Expensas e Impuestos Provinciales y/o Municipales</b> que recaigan sobre el inmueble, hasta un <b>30 %% del monto total del contrato</b>;  <b>Ocupación Indebida hasta 6 meses de finalizado el contrato</b>. Todo lo expuesto con arreglo a las condiciones generales y particulares que forman parte integrante de la póliza de seguro y que <b>LOCADOR</b> y <b>LOCATARIO</b> aceptan al celebrar el presente.".format(fianza_o_garantia, numero_poliza)
                text12 = '''
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                '''.format(toInsert)
            elif option == 5:
                toInsert = u"<b><u>DÉCIMA SEGUNDA{}:</u></b> En garantía del fiel cumplimiento del pago de los alquileres indicados en este contrato, <b>la LOCATARIA</b> ofrece una póliza de caución de <b>Alba Caución Compañía Argentina de Seguros</b>, con domicilio en <b>Avenida Belgrano 875 - Ciudad Autónoma de Buenos Aires</b>, N° de póliza <b>{}</b> con vigencia a partir del <b>{} de {} {}</b>, a favor de <b>la LOCADORA</b> por el monto y duración del contrato. En virtud de ello, <b>la LOCATARIA</b> ofrece a <b>la LOCADORA</b>, y esta acepta, la garantía referida de acuerdo a las siguientes condiciones: una póliza de caución de <b>Alba Caución Compañía Argentina de Seguros </b> a favor de <b>la LOCADORA</b> que garantizará el pago de los alquileres emergentes del presente contrato durante el término de <b>los 24 meses</b> de duración de la locación.".format(fianza_o_garantia, numero_poliza, xx, mes, ano)
                text12 = '''
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                '''.format(toInsert)
            elif option == 6:

                toInsert1 = u"<b><u>DÉCIMA SEGUNDA{}:</u> A.-</b> Constituyendo domicilio especial en <b>Av.​ Rivadavia 3855 1º Piso de​ la Ciudad Autónoma de Buenos Aires</b>, donde serán válidas todas las notificaciones que se le remitan, representada en este acto por <b>{},​ DNI {}</b> en su calidad de apoderado que acredita acompañando el <b>Poder Especial de SISTEMA​ FINAER S.A.</b> procede a suscribir el presente contrato de locación al único efecto de asumir la calidad de fiador del Locatario, renunciando a oponer tanto el beneficio de la división de la deuda como el de excusión previa, de acuerdo al <b>Contrato de Garantía</b> que integra al presente contrato de locación y se incorpora como <b>Anexo​ N°: 1</b> al​ presente y que todos los intervinientes declaran conocer y aceptar. La presente Fianza es un contrato accesorio que comenzará a regir a partir de la fecha en que se suscribe el presente contrato de locación y comprende estrictamente el plazo de vigencia que el presente instrumento de locación acuerda, o la restitución efectiva del bien locado. Cualquier modificación que <b>Locador y Locatario</b> introdujeren al contrato principal sin aceptación expresa del <b>Fiador</b> le resultará inoponible a éste; en base a ello <b>SISTEMA FINAER S.A.</b> queda obligada a poner a disposición del propietario locador, o la persona que lo represente legalmente, los importes dinerarios que correspondan a los eventuales incumplimiento del Locatario en la extensión prevista en el contrato de garantía otorgado. Tales importes (​ Canon mensual; Expensas ordinarias; ABL; Agua, con sus correspondientes punitorios hasta <b>el 0,5 %% diario</b> no capitalizable todo ello de conformidad con las condiciones fijadas en el <b>Anexo Nº 1</b>) quedarán a disposición del Locador luego de recibidas las denuncias de cada incumplimiento imputable al <b>Locatario</b> que debe elaborar <b>el Locador</b>, por escrito y que deberán ser presentadas en el domicilio especial del fiador. Los importes abonados por el <b>Fiador al Locador</b> son objeto de subrogación automática, quedando autorizado <b>el Fiador</b> por si o por <b>el Locador</b> a promover su ejecución por la vía ejecutiva o demandar otras garantías otorgadas por el Locatario a su arbitrio. La cancelación del canon mensual por parte del fiador como obligación accesoria e independiente no puede purgar el estado de mora del Locatario ni obstar la promoción de tal acción de desalojo.".format(fianza_o_garantia, nombre_finaer, documento_representanto)

                toInsert2 = u"<b>B.-</b> Es condición esencial previa al pago del segundo incumplimiento por parte del Fiador que el Locador otorgue mandato judicial a favor del o los profesionales que el Fiador indique, pudiendo éste unificar su representación legal. En tales casos los costos y costas de la representación profesional judicial, como los gastos, sean judiciales o extrajudiciales, son a cargo del Fiador, quien los repetirá del Locatario. El mandato que ha de otorgar el Locador contendrá la facultad de representación para enviar todo tipo de intimaciones y requerimientos, incluso las que el art. 1222 del Código Civil y Comercial de la Nación, declarar rescindido el contrato principal, demandar por desalojo por cualquier causal, como la de promover demandas por cobro de pesos; efectuar todo tipo de reclamo dinerario; a reclamar la entrega y recibir el inmueble locado y sus llaves; de constatar su estado de ocupación y físico; y proceder a su ulterior entrega al Locador; y respecto a las sumas abonadas por el fiador los derechos se encuentra cedidos quedando el Fiador en la misma situación jurídica frente al Locatario que el Locador, pudiendo ejercer todas y cada una de las acciones y derechos que el Locador detenta y surgen del contrato de locación. Locador y Locatario autorizan y dan expresa conformidad, sin que ello implique modificación alguna a lo establecido en el contrato de locación suscripto por las partes, al otorgamiento del mandato aludido siendo las facultades descriptas meramente enunciativas."

                toInsert3 = u"<b>C.-</b> Queda expresamente establecido que si durante el curso del plazo del contrato de locación garantido las partes decidieren modificar las cláusulas relativas al precio de la locación, ellas deberán notificar previamente al Fiador de modo fehaciente, quien tendrá derecho a aceptar tal modificación repactando su acuerdo con el Locatario debiendo éste afrontar el mayor costo que tal modificación implica, o de rechazar tal modificación manteniendo sólo la extensión de su fianza en su garantía original. Queda establecido que el silencio <b>del Fiador</b> ante cualquier modificación del acuerdo original celebrado entre las partes no podrá ser considerado aceptación expresa ni implícita de lo acordado, y por lo tanto, conformidad con el aumento o extensión de la fianza original oportunamente suscripta."

                toInsert4 = u"Los intervinientes en el presente contrato establecen que los domicilios que indican son domicilios especiales circunstancia por la cual las notificaciones a ellos dirigida son plenamente válidas y surtirán plenos efectos legales, aun cuando las partes no se encuentren en ellos de modo permanente o transitorio. Los domicilios especiales aquí indicados podrán ser modificados previa notificación fehaciente a todas las partes, revistiendo siempre los nuevos domicilios también la calidad de domicilios especiales con los efectos antes descriptos."

                toInsert5 = u"Ninguna disposición del contrato de locación vinculada a <b>la fianza</b> puede contrariar la presente cláusula y las contenidas en <b>el Anexo I</b> del presente, so pena de nulidad absoluta."

                text12 = '''
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-234px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-252px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-252px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-270px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-270px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-288px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-288px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-306px;">
                        <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
                </div>
                <div align="justify" STYLE="position:relative; TOP:-306px;">
                        <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
                </div>
                '''.format(toInsert1, toInsert2, toInsert3, toInsert4, toInsert5)

            # Decima Tercera
            text13 = u"<b><u>DÉCIMA TERCERA - RESOLUCIÓN ANTICIPADA:</u></b> El​ contrato de locación puede ser resuelto anticipadamente por <b>el Locatario</b>, en los términos <b>del art. 1221 del Código Civil y Comercial de la Nación</b>, si han transcurrido seis meses de contrato, debiendo el locatario notificar en forma fehaciente su decisión <b>al Locador</b>. Si hace uso de esta opción resolutoria en el primer año de vigencia de la relación locativa, debe abonar al locador, en concepto de indemnización, la suma equivalente a un mes y medio de alquiler al momento de desocupar el inmueble y la de un mes si la opción se ejercita transcurrido dicho lapso."

            # Decima Cuarta
            text14 = u"<b><u>DÉCIMA CUARTA - DEPÓSITO EN GARANTÍA:</u></b> A fin de garantizar el fiel cumplimiento de este contrato y de todas las obligaciones contraídas por <b>\"EL​ LOCATARIO\"</b>,​ este entrega en calidad de depósito la suma de PESOS​ <b>{}</b>  <b>(${})</b> sirviendo el presente contrato como carta de pago y suficiente recibo. Este depósito no devengará interés alguno, ni podrá <b>\"EL​ LOCATARIO\"</b> imputarlo al pago de alquileres reservándose <b>\"EL​ LOCADOR\"</b> el derecho de cancelar con el mismo cualquier deuda pendiente al finalizar el contrato. El depósito será devuelto transcurridos sesenta Días (60) de haberse reintegrado la propiedad completamente desocupada y a entera satisfacción de <b>\"EL​ LOCADOR\"</b>,​ previo las deducciones a que hubiere lugar.".format(texto_monto_deposito, numero_monto_deposito)

            # Decima Quinta
            text15 = u"<b><u>DÉCIMA QUINTA- RESTITUCIÓN:</u></b> Al vencimiento del término pactado en el presente <b>\"EL​ LOCATARIO\"</b> se obliga a entregar la finca alquilada a <b>\"EL​ LOCADOR\"</b> previa indemnización de los desperfectos que existieran, no admitiendose la tácita reconducción del contrato aún cuando <b>\"EL​ LOCATARIO\"</b> continuará ocupando lo arrendado."

            # Decima Sexta
            text16 = u"<u><b>DÉCIMA SEXTA- REGLAMENTO: </u>\"EL LOCATARIO\"</b> declara​ conocer y se obliga a cumplir con las disposiciones legales vigentes sobre propiedad horizontal y las establecidas en el reglamento interno del edificio."

            # Decima Septima
            text17 = u"<b><u>DÉCIMA SÉPTIMA- CONVENIO DE DESOCUPACIÓN:</u></b> Dentro de <b>los ciento ochenta (180) días anteriores</b> a la finalización del plazo contractual, <b>\"EL​ LOCADOR\"</b> podrá exigir a <b>\"EL​ LOCATARIO\"</b> la firma de un convenio de desocupación y solicitar su posterior homologación judicial, que faculte a <b>\"EL​ LOCADOR\"</b> para diligenciar el lanzamiento judicial al vencimiento del plazo contractual. Si <b>\"EL​ LOCATARIO\"</b> se negase a firmar dicho convenio y <b>\"EL LOCADOR\"</b> se ve obligado a promover la demanda anticipada de desalojo las costas serán por cuenta de <b>\"EL​ LOCATARIO\"</b> aunque restituya el inmueble, sin perjuicio de la aplicación de la multa en el caso de demora en la restitución."

            # Decima Octava
            text18 = u"<b><u>DÉCIMA OCTAVA- INVENTARIO:</b></u> Se ajunta <b>ANEXO INVENTARIO</b> al mismo que ambas partes declaran conocer."

            # Decima Novena
            text19 = u"<b><u>DÉCIMA NOVENA- SEGURO INCENDIO:</b></u> <b>El locatario</b> se compromete a contratar un seguro de ​ incendio​ y​ responsabilidad civil, sobre el inmueble del presente, por el periodo que dure este contrato​, endosando la póliza a nombre <b>del Locador</b>."

            # Vigesima
            text20 = u"<b><u>VIGÉSIMA  - JURISDICCIÓN Y DOMICILIOS:</b></u> Todas​ las partes se someten a la jurisdicción de la justicia ordinaria de <b>la Capital Federal</b> con exclusión del fuero federal, en caso de ser procedente, constituyendo domicilios especiales en los citados anteriormente, donde cada una de las partes tendrá por válidas las notificaciones judiciales y/o extrajudiciales, aún cuando no viviesen más en ellos y hasta tanto no comuniquen fehacientemente sus nuevos domicilios, renunciando además al derecho a recusar sin causa <b>al Juzgado y/o Tribunal interviniente</b>."

            # Closing clause
            textClose = u"En prueba de conformidad se firman <b>tres (3)</b> ejemplares de un mismo tenor y a un solo efecto en <b>la Ciudad de Buenos Aires</b>, a los ____ días del mes de ________ de ________."

            if not option == 6:
                count13 = "252"
                count14 = "270"
                count15 = "288"
                count16 = "306"
                count17 = "324"
                count18 = "342"
                count19 = "360"
                count20 = "379"
                count21 = "396"
            else:
                count13 = "324"
                count14 = "342"
                count15 = "360"
                count16 = "378"
                count17 = "396"
                count18 = "414"
                count19 = "432"
                count20 = "450"
                count21 = "468"
            tituloo = "CONTRATO DE LOCACIÓN"
            html = u'''
            <!DOCTYPE html>
            <html>
            <meta charset="UTF-8" />
            <head>
            <style>
                div  {{ margin:0px; padding:0px; margin-top: 0px; margin-bottom: 0px;}}
                span  {{margin:0px; padding:0px;margin-top: 0px; margin-bottom: 0px;}}
                h3 {{margin-bottom: 50px;}}
            </style>
            </head>
            <body>

            <!--Title-->
            <h3 align="center"><u>{}</u></h3>

            <!--Intro-->
            <div align="justify" STYLE="position:relative; TOP:-18px;">
                    <span style="background-color: #FFFFFF; z-index: 1;" >{}</span>
            </div>
            <div align="justify">
                    <span  STYLE="position:relative; TOP:-36px;z-index:-1;position:relative">---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Primera-->
            <div align="justify" STYLE="position:relative; TOP:-36px;">
                    <span style="background-color: #FFFFFF; z-index: 1;">{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-36px;" >
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;position:relative">---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>


            <!--Segunda-->
            <div align="justify" STYLE="position:relative; TOP:-54px">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-54px">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>


            <!--Tercera-->
            <div align="justify" STYLE="position:relative; TOP:-72px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-72px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>


            <!--Cuarta-->
            <div align="justify" STYLE="position:relative; TOP:-90px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-90px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Quinta-->
            <div align="justify" STYLE="position:relative; TOP:-108px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-108px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Sexta-->
            <div align="justify" STYLE="position:relative; TOP:-126px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-126px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Septima-->
            <div align="justify" STYLE="position:relative; TOP:-144px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-144px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Octava-->
            <div align="justify" STYLE="position:relative; TOP:-162px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-162px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Novena-->
            <div align="justify" STYLE="position:relative; TOP:-180px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-180px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima-->
            <div align="justify" STYLE="position:relative; TOP:-198px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-198px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima Primera-->
            <div align="justify" STYLE="position:relative; TOP:-216px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-216px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima segunda-->
            {}

            <!--Decima tercera-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima Cuarta-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima Quinta-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima Sexta-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima Septima-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima Octava-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Decima Novena-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Vigesima-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            <!--Closing Text-->
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span style="background-color: #FFFFFF"; z-index: 1;>{}</span>
            </div>
            <div align="justify" STYLE="position:relative; TOP:-{}px;">
                    <span  STYLE="position:relative; TOP:-18px;z-index:-1;"; position:relative>---------------------------------------------------------------------------------------------------------------------------------------------------</span>
            </div>

            </body>
            </html>
            '''.format(tituloo, textIntro, text1, text2, text3, text4, text5, text6, text7, text8, text9, text10, text11, text12, count13, text13, count13, count14, text14, count14, count15, text15, count15, count16, text16, count16, count17, text17, count17, count18, text18, count18, count19, text19, count19, count20, text20, count20, count21, textClose, count21)


            f = open('page.html','w')
            f.write(html)
            f.close()

            # os.system("pwd")
            # os.system("cd /app/.heroku/python/lib/python3.6/site-packages/pdfkit")
            # os.system("cd /app/.heroku/python/lib/python3.6/site-packages/pdfkit")
            # os.system("ls")
            # os.system("echo there")

            # resultFile = open('contract.pdf', "w+b")
            # convert HTML to PDF

            options = {
                'margin-top': '.5in',
                'margin-right': '.8in',
                'margin-bottom': '.5in',
                'margin-left': '.8in',
            }

            config = pdfkit.configuration(wkhtmltopdf="wkhtmltopdf")
            pdfkit.from_file('page.html', 'contract.pdf', configuration=config, options=options)
            filepath = 'contract.pdf'
            return serve(request, os.path.basename(filepath), os.path.dirname(filepath))

            # pdf = HTML(string=html.encode('utf-8'))
            # pdf.write_pdf()


    form = ProfileForm()
    context = {
        'form': form,
    }
    return render(request, 'contract/index.html', context)


######################################################################################################################

def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'name.html', {'form': form})


class DetailView(generic.DetailView):
    model = Question
    template_name = 'contract/detail.html'

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'contract/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'contract/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('contract:results', args=(question.id,)))