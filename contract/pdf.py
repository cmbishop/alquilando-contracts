# -*- encoding: utf-8 -*-
import sys
from fpdf import FPDF

sys.setdefaultencoding('utf8')


pdf.set_fill_color(r=255, g=255, b=255)

# Prints title
pdf.add_page()
pdf.set_font("Arial", 'BU', size=12)
pdf.cell(w=200, h=12, txt=u"Requerimiento para automatización de contratos de locación", ln=1, align="C")
